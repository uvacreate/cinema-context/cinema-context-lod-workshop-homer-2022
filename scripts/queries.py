import os


def get_queries(folder_query: str = "./queries"):

    filenames_queries = []

    for filename in os.listdir(folder_query):
        if not filename.endswith(".rq"):
            continue

        queryfilepath = os.path.join(folder_query, filename)
        with open(queryfilepath, "r") as infile:
            query = infile.read()

        filenames_queries.append((filename, query))

    return filenames_queries
