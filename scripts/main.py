import os
import json
from itertools import count

import pandas as pd

import rdflib
from rdflib import Graph, Namespace, URIRef, Literal, RDF, OWL, XSD, SDO
from rdflib.resource import Resource

# Namespaces
LODHOMER = Namespace(
    "https://uvacreate.gitlab.io/cinema-context/cinema-context-lod-workshop-homer-2022/id/"
)
GEO = Namespace("http://www.opengis.net/ont/geosparql#")
IMDB = Namespace("https://www.imdb.com/title/")
WD = Namespace("http://www.wikidata.org/entity/")


def read_speadsheet(filepath: str) -> pd.DataFrame:
    """
    Reads in a spreadsheet in csv or xslx format and returns it as a Pandas DataFrame.

    Args:
        filepath (str): Path to the spreadsheet

    Raises:
        Exception: Raised when path is pointing to a non-spreadsheet file.

    Returns:
        pd.DataFrame: DataFrame with the contents of the spreadsheet
    """

    _, extension = os.path.splitext(filepath)

    if extension == ".csv":
        df = pd.read_csv(filepath, encoding="utf-8")
    elif extension == ".xslx":
        df = pd.read_excel(filepath)
    else:
        raise Exception("File has the wrong extension. Only .csv and .xslx allowed!")

    return df


def convert_to_rdf(df: pd.DataFrame, graph_identifier: str) -> rdflib.Graph:
    """
    Convert the spreadsheet to RDF in a given RDF Data Model (schema.org).

    This function converts the given spreadsheet, with fixed column names, 
    to our determined RDF data format. We use most of the schema.org 
    vocabulary for this (see: https://uvacreate.gitlab.io/cinema-context/cinema-context-lod-workshop-homer-2022/model/).

    We expect that the spreadsheet contains the following columns:
     - screening_id
     - screening_date
     - film_id
     - film_name
     - film_name_language
     - film_imdb
     - film_wikidata
     - theater_id
     - theater_name
     - theater_address
     - theater_coordinates
     - theater_city
     - theater_country

    Args:
        df (pd.DataFrame): Spreadsheet 
        graph_identifier (str): _description_

    Returns:
        rdflib.Graph: _description_
    """

    g = Graph(identifier=graph_identifier, store="Oxigraph")
    g.bind("geo", GEO)
    g.bind("schema", SDO)
    g.bind("wd", WD)
    g.bind("imdb", IMDB)

    screeningCounter = count(1)

    filmCounter = count(1)
    film2id = dict()

    theaterCounter = count(1)
    theater2id = dict()

    for r in df.to_dict(orient="records"):

        nsDataset = Namespace(graph_identifier)

        # schema:ScreeningEvent
        if pd.isna(r["screening_id"]):
            screening_id = next(screeningCounter)
        else:
            screening_id = r["screening_id"]
        screeningURI = nsDataset.term(f"screening/{screening_id}")

        screening = Resource(g, screeningURI)
        screening.add(RDF.type, SDO.ScreeningEvent)

        screening_date = r["screening_date"]
        screening.add(SDO.startDate, Literal(screening_date, datatype=XSD.date))

        # schema:Movie
        if pd.isna(r["film_id"]):
            if not pd.isna(r["film_imdb"]):
                film_id = r["film_imdb"]
            elif r["film_name"] in film2id:
                film_id = film2id[r["film_name"]]
            else:
                film_id = next(filmCounter)
                film2id[r["film_name"]] = film_id
        else:
            film_id = r["film_id"]

        movieURI = nsDataset.term(f"film/{film_id}")

        movie = Resource(g, movieURI)
        movie.add(RDF.type, SDO.Movie)

        movie_name = r["film_name"]
        if pd.isna(r["film_name_language"]):
            movie.add(SDO.name, Literal(movie_name))
        else:
            movie.add(SDO.name, Literal(movie_name, lang=r["film_name_language"]))

        if not pd.isna(r["film_imdb"]):
            imdbURI = IMDB.term(r["film_imdb"])
            movie.add(SDO.sameAs, imdbURI)

        if not pd.isna(r["film_wikidata"]):
            wdURI = WD.term(r["film_wikidata"])
            movie.add(OWL.sameAs, wdURI)

        # Specific links
        if "film_egcd" in r and not pd.isna(r["film_egcd"]):
            movie.add(SDO.sameAs, URIRef(r["film_egcd"]))
        if "film_filmportal" in r and not pd.isna(r["film_filmportal"]):
            movie.add(SDO.sameAs, URIRef(r["film_filmportal"]))

        # schema:MovieTheater
        if pd.isna(r["theater_id"]):
            unique_theater_combination = tuple(
                [
                    i
                    for i in [
                        r["theater_name"],
                        r["theater_address"],
                        r["theater_coordinates"],
                        r["theater_city"],
                        r["theater_country"],
                    ]
                    if not pd.isna(i)
                ]
            )
            if unique_theater_combination in theater2id:
                theater_id = theater2id[unique_theater_combination]
            else:
                theater_id = next(theaterCounter)
                theater2id[unique_theater_combination] = theater_id
        else:
            theater_id = r["theater_id"]

        theaterURI = nsDataset.term(f"theater/{theater_id}")

        theater = Resource(g, theaterURI)
        theater.add(RDF.type, SDO.MovieTheater)

        theater_name = r["theater_name"]
        theater.add(SDO.name, Literal(theater_name))

        ## Address
        addressURI = theaterURI + "#address"
        address = Resource(g, addressURI)
        address.add(RDF.type, SDO.PostalAddress)

        if not pd.isna(r["theater_address"]):
            address_address = r["theater_address"]
            address.add(SDO.streetAddress, Literal(address_address))
        else:
            address_address = None

        theater_city = r["theater_city"]
        address.add(SDO.addressLocality, Literal(theater_city))

        theater_country = r["theater_country"]
        address.add(SDO.addressCountry, Literal(theater_country))

        theater.add(SDO.address, address)

        if address_address:
            address_name = f"{address_address}, {theater_city}, {theater_country}"
        else:
            address_name = f"{theater_city}, {theater_country}"

        address.add(SDO.name, Literal(address_name))

        ## Coordinates
        if not pd.isna(r["theater_coordinates"]):
            lat, long = r["theater_coordinates"].split(",")

            geometryURI = theaterURI + "#geometry"
            geometry = Resource(g, geometryURI)
            geometry.add(RDF.type, GEO.Geometry)

            wkt = f"POINT({long} {lat})"
            geometry.add(GEO.asWKT, Literal(wkt, datatype=GEO.wktLiteral))

            theater.add(GEO.hasGeometry, geometry.identifier)

        # Relations
        screening.add(SDO.workPresented, movie)
        screening.add(SDO.location, theater)

        # Let's also create a name for the screeningEvent
        screening_name = f"Screening of {movie_name} at {theater_name}, {theater_city}, {theater_country} ({screening_date})"
        screening.add(SDO.name, Literal(screening_name))

    return g


def main(
    source_folder="../docs/data/datasets/original/",
    metadata_folder="../docs/data/datasets/metadata/",
    ns=LODHOMER,
    destination_folder="../docs/data/datasets/rdf/",
):

    # Metadata
    for filename in os.listdir(metadata_folder):
        metafilepath = os.path.join(metadata_folder, filename)

        with open(metafilepath, "r", encoding="utf-8") as jsonfile:
            metadata = json.load(jsonfile)

        dataset_name = metadata["name"]
        graph_identifier = metadata["@id"]
        slug = graph_identifier.replace(ns, "")[:-1]  # no trailing slash

        print("Processing", dataset_name)

        filename = metadata["distribution"][0]["contentUrl"].replace(
            "https://uvacreate.gitlab.io/cinema-context/cinema-context-lod-workshop-homer-2022/data/datasets/original/",
            "",
        )

        # Convert spreadsheet data to RDF

        filepath = os.path.join(source_folder, filename)
        df = read_speadsheet(filepath)

        # Convert to RDF
        if df is not None:
            g = convert_to_rdf(df, graph_identifier)

            dest_file = os.path.join(
                destination_folder, f"workshop_lod_homer2022_{slug}.trig"
            )
            print("Serializing to", dest_file)
            g.serialize(dest_file, format="trig")

        print("Done!\n")


if __name__ == "__main__":
    main()
