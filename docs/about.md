# About

This Linked Open Data workshop is meant as a demonstration of the research potential of bringing different cinema datasets together as Linked Open Data (LOD/RDF) for the participants of the HoMER 2022 conference. 

## Acknowledgements
We thank all the participants of the workshop for their contributions to the workshop and the fruiteful discussions during the workshop. We also thank the HoMER 2022 conference for the opportunity to organise this workshop, as well as the Sapienza University of Rome for hosting us.

Special thanks to the following people for contributing their data. Without their data, this workshop would not have been possible: Ainamar Clariana-Rodagut, Anastasia Balukova, Åsa Jernudd, Christophe Verbruggen, Clara Pafort-Overduin, Daniël Biltereyst, Dries Moreels, Ivan Karnaukhov, Julia Noordegraaf, Kristina Tanis, Philippe Meers and Pierre Stotzky.



## Deliverables
All the results of the workshop are available in this repository. The data of the participants that gave permission to publish their data can be found on the [results](./results) page. Here you find the original datasets (in CSV), metadata on the datasets (in JSON-LD) and the Linked Open Data that was created for the workshop (in RDF/TriG).

## Contact
For questions about the workshop, please contact CREATE (University of Amsterdam): [createlab@uva.nl](mailto:createlab@uva.nl)
