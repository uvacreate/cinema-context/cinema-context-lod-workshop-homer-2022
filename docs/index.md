# Workshop
## Linking Cinema Data. An interactive workshop on bridging multiple cinema datasets through Linked Open Data

**Wednesday 6th July 2022, 14.00-16.00h, Sapienza Università di Roma**

In this interactive workshop we will bring together cinema datasets as Linked Open Data (LOD) and explore the research potential of this approach.

Organizers:

* Julia Noordegraaf (University of Amsterdam)
* Leon van Wissen (University of Amsterdam)
* Ivan Kisjes (University of Amsterdam)
* Thunnis van Oort (Radboud University) 
* Clara Pafort-Overduin (Utrecht University)

### Introduction

During the HoMER 2021 Karel Dibbets panel on “Digital Data, Platforms And New Cinema History” we recognized a desire to increase collaboration between researchers working on cinema datasets of the same type with similar/reusable data models. In fact, the vision of interoperable and interchangeable datasets that New Cinema Historians can collectively research, is one of the founding principles of the HoMER community (Klenotic 2020). To contribute to such a collaboration, we propose an interactive workshop for HoMER 2022 to bring together cinema datasets as Linked Open Data (LOD) and explore the research potential of this approach.

The Cinema Context dataset, recently [published as Linked Open Data](https://uvacreate.gitlab.io/cinema-context/cinema-context-rdf/) (Van Wissen et al. 2021) serves as a good starting point for this workshop, in which we will also convert other, similarly structured data on local cinema cultures to Linked Open Data. We consider the data from the [European Cinema Audiences](https://www.dhi.ac.uk/eca/map) project, [Cinema Belgica](https://www.cinemabelgica.be/), and the [Movie Theatres in Wartime](https://www.create.humanities.uva.nl/events/movie-theatres-in-wartime/) initiative as examples to show what this method brings, but we strongly invite and encourage workshop participants to bring and supply their own structured (tabular) data on film screenings beforehand.

The workshop consists of two parts. In the first, online, pre-conference part, we will focus on the data preparation process and the data model, for which the workshop organizers will provide support. In the second part, which will take place at the HoMER conference, we will focus on working with the linked data and writing data stories that showcase the types of research questions that we can pose to combined data sets.

In the pre-conference part, we will start with an introduction on the principles of Linked Open Data, outline the data model used for our conversion of Cinema Context to LOD, and discuss the data preparation process. We will walk participants through the conversion to LOD. During the workshop at the HoMER conference, we are then ready to query the linked datasets and test them by formulating research questions that would exceed the scope of a single dataset. We will also show how the Linked Open Data format opens up possibilities to link to other, external datasets, such as Wikidata (e.g., for contextual information on cinemas and movies). During and after the workshop, we ask participants to write a brief data story about the datasets, the process of linking them and the research it allows us for, which will be published on this website as a lasting deliverable of the workshop.

#### Registration

The link to register for the workshop can be found here: [https://homernetwork.org/workshops/](https://homernetwork.org/workshops/). Participation is limited to delegates of the HoMER Conference that will take place in Rome (July 5-8, 2022).

#### Preconference workshop

On Wednesday 8 June we held a preconference workshop online to provide some general information on Linked Open Data, to give some tips on how to enrich your data and to explain how you can deliver the data for the on-location workshop in Rome. If you could not be present at this preconference meeting, you can still attend the workshop in Rome. The slides of this meeting can be found [here](./data/Workshop_LOD_HoMER_2022_preconference.pdf). The meeting was recorded, if you want to receive the video of the zoom-meeting, please send an email to [createlab@uva.nl](mailto:createlab@uva.nl). If you have any further questions you can use the same mail address.


#### References

* Cinema Belgica: [https://www.cinemabelgica.be](https://www.cinemabelgica.be/)

*  Cinema Context RDF documentation, [https://uvacreate.gitlab.io/cinema-context/cinema-context-rdf/](https://uvacreate.gitlab.io/cinema-context/cinema-context-rdf/)

* Dibbets, K. 2010. Cinema Context and the genes of film history. _New Review of Film and Television Studies_ 8(3), 331-342.

* Dibbets, K. 2018. Cinema Context. _Film in Nederland vanaf 1896: Een encyclopedie van de filmcultuur_. [https://doi.org/10.17026/dans-z9y-c5g6](https://doi.org/10.17026/dans-z9y-c5g6)

* European Cinema Audiences: [https://www.dhi.ac.uk/eca/map](https://www.dhi.ac.uk/eca/map)

* Klenotic, Jeffrey. 2020. Mapping Flat, Deep, and Slow: On the ‘Spirit of Place’ in New Cinema History. _TMG Journal for Media History_ 23 (1-2): 1–34. DOI: [http://doi.org/10.18146/tmg.789](http://doi.org/10.18146/tmg.789)

* Van Wissen, L., Van Oort, T., Noordegraaf, J., Kisjes, I. 2021. Cinema Context as Linked Open Data: Converting an online Dutch film culture dataset to RDF. In I. Tiddi, M. Maleshkova, T. Pellegrini, V. de Boer, eds. _Joint Proceedings of the Semantics co-located events: Poster&Demo track and Workshop on Ontology-Driven Conceptual Modelling of Digital Twins co-located with Semantics 2021, Amsterdam, The Netherlands, September 6-9, 2021_.  CEUR Workshop Proceedings 2941, CEUR-WS.org. [http://ceur-ws.org/Vol-2941/paper10.pdf](http://ceur-ws.org/Vol-2941/paper10.pdf)
