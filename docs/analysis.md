# Analysis

Reserved for during and after the workshop. To get an idea of the type of questions that we can ask, you can take a look at the [example queries of Cinema Context in RDF](https://uvacreate.gitlab.io/cinema-context/cinema-context-rdf/queries/overview/). 

!!! warning 
    In order to query the data, you need a triple store to host and serve the datasets. During the workshop, we will be using the (free) TriplyDB service that allows to browse and query the data: [https://triplydb.com/LvanWissen/LOD-HoMER2022](https://triplydb.com/LvanWissen/LOD-HoMER2022)

    Queries can be written here: [https://triplydb.com/LvanWissen/LOD-HoMER2022/sparql/LOD-HoMER2022](https://triplydb.com/LvanWissen/LOD-HoMER2022/sparql/LOD-HoMER2022)

## Queries and results
### Descriptive statistics

??? Example "All datasets submitted"
    === "SPARQL"
    ```sparql
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX schema: <https://schema.org/>
    SELECT * WHERE {
    ?dataset a schema:Dataset ;
        schema:name ?name .
    
    } ORDER BY ?name
    ```

    Result:

    |dataset|name                                         |
    |-------|---------------------------------------------|
    |https://uvacreate.gitlab.io/cinema-context/cinema-context-lod-workshop-homer-2022/id/cinemabelgica/|Cinema Belgica                               |
    |https://uvacreate.gitlab.io/cinema-context/cinema-context-lod-workshop-homer-2022/id/cinemacontext/|Cinema Context                               |
    |https://uvacreate.gitlab.io/cinema-context/cinema-context-lod-workshop-homer-2022/id/filmclub/|Film Club Events                             |
    |https://uvacreate.gitlab.io/cinema-context/cinema-context-lod-workshop-homer-2022/id/netherlands/|Film Programming The Netherlands 1934-1936   |
    |https://uvacreate.gitlab.io/cinema-context/cinema-context-lod-workshop-homer-2022/id/buenosaires/|Historia de los públicos de cine en Buenos Aires|
    |https://uvacreate.gitlab.io/cinema-context/cinema-context-lod-workshop-homer-2022/id/metzww1/|Metz World War I                             |
    |https://uvacreate.gitlab.io/cinema-context/cinema-context-lod-workshop-homer-2022/id/moscow/|Moscow film programming                      |
    |https://uvacreate.gitlab.io/cinema-context/cinema-context-lod-workshop-homer-2022/id/amsterdam/|Screenings Amsterdam 1952, 1962 and 1972     |
    |https://uvacreate.gitlab.io/cinema-context/cinema-context-lod-workshop-homer-2022/id/orebro/|Örebro films screened 1956-1958 and 1966-1968|    

??? Example "Counts of screenings, films and theaters per dataset"
    === "SPARQL"
    ```sparql
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX schema: <https://schema.org/>

    # Group by dataset and give counts of unique screenings, films, theaters
    SELECT ?dataset ?name (COUNT(DISTINCT ?screening) AS ?n_screenings) (COUNT(DISTINCT ?film) AS ?n_films) (COUNT(DISTINCT ?theater) AS ?n_theaters) WHERE {
    
    # Metadata
    ?dataset a schema:Dataset ;
        schema:name ?name .
    
    # Content
    GRAPH ?dataset {    
        
        # A screening has a workPresented and a location .
        ?screening a schema:ScreeningEvent ;
            schema:workPresented ?film ;
            schema:location ?theater .
        
        # The film is of a type 'https://schema.org/Movie'
        ?film a schema:Movie .    

        # The theater is of a type 'https://schema.org/MovieTheater'
        ?theater a schema:MovieTheater .
    } 

    } GROUP BY ?dataset ?name ORDER BY ?name
    ```

    Result:

    |dataset                                                                                            |name                                         |n_screenings|n_films|n_theaters|
    |---------------------------------------------------------------------------------------------------|---------------------------------------------|------------|-------|----------|
    |https://uvacreate.gitlab.io/cinema-context/cinema-context-lod-workshop-homer-2022/id/cinemabelgica/|Cinema Belgica                               |26073       |7830   |200       |
    |https://uvacreate.gitlab.io/cinema-context/cinema-context-lod-workshop-homer-2022/id/cinemacontext/|Cinema Context                               |24345       |8064   |218       |
    |https://uvacreate.gitlab.io/cinema-context/cinema-context-lod-workshop-homer-2022/id/netherlands/  |Film Programming The Netherlands 1934-1936   |23679       |2403   |145       |
    |https://uvacreate.gitlab.io/cinema-context/cinema-context-lod-workshop-homer-2022/id/metzww1/      |Metz World War I                             |456         |437    |3         |
    |https://uvacreate.gitlab.io/cinema-context/cinema-context-lod-workshop-homer-2022/id/moscow/       |Moscow film programming                      |13782       |372    |60        |
    |https://uvacreate.gitlab.io/cinema-context/cinema-context-lod-workshop-homer-2022/id/amsterdam/    |Screenings Amsterdam 1952, 1962 and 1972     |8379        |2994   |49        |
    |https://uvacreate.gitlab.io/cinema-context/cinema-context-lod-workshop-homer-2022/id/orebro/       |Örebro films screened 1956-1958 and 1966-1968|37741       |2839   |80        |



??? Example "Statistics: Counts of 'types' (very generic query)"
    === "SPARQL"
    ```sparql

    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX schema: <https://schema.org/>

    # Give me everything of _a_ type and count the number of instances of that specific type.
    SELECT (COUNT(?entity) AS ?count) ?type WHERE {  
        ?entity a ?type .  #something we call 'entity' is of a specific type
    } GROUP BY ?type ORDER BY DESC(?count)
    
    ```

    Result:

    |count |type                                         |
    |------|---------------------------------------------|
    |134455|https://schema.org/ScreeningEvent            |
    |24939 |https://schema.org/Movie                     |
    |755   |https://schema.org/PostalAddress             |
    |755   |https://schema.org/MovieTheater              |
    |499   |http://www.opengis.net/ont/geosparql#Geometry|
    |48    |https://schema.org/Person                    |
    |18    |https://schema.org/DataDownload              |
    |12    |https://schema.org/Organization              |
    |10    |https://schema.org/Place                     |
    |9     |https://schema.org/Dataset                   |
    |1     |https://schema.org/DataCatalog               |

    This is a combination of counts from the data and the metadata. For instance, there are no `https://schema.org/Place` instances modelled in the dataset, since cities are included as 'string', according to the [data model](model/index.md). The same holds for `https://schema.org/Person` instances.


??? Example "Most popular films per dataset"

    This example query is written on the Metz dataset (URI: `https://uvacreate.gitlab.io/cinema-context/cinema-context-lod-workshop-homer-2022/id/metzww1/`) and gives back the 10 most popular films by number of screenings. 

    === "SPARQL"
    ```sparql
    PREFIX schema: <https://schema.org/>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    SELECT ?film_name (COUNT(?screening) AS ?n_screenings) WHERE {
    GRAPH <https://uvacreate.gitlab.io/cinema-context/cinema-context-lod-workshop-homer-2022/id/metzww1/> {
        ?screening a schema:ScreeningEvent ;
            schema:workPresented ?film .
        
        ?film a schema:Movie ;
            schema:name ?film_name .
    }
    } ORDER BY DESC(?n_screenings) LIMIT 10
    ```

    Result:

    |film_name|n_screenings                        |
    |---------|------------------------------------|
    |Aus Deutschlands Ruhmestagen 2. Teil Vater und Sohn|3                                   |
    |Das Todesschiff|3                                   |
    |Der Millionentanz|3                                   |
    |Aus Deutschlands Ruhmestagen|3                                   |
    |Vermisst gemeldet|2                                   |
    |Hemngekehrt|2                                   |
    |Teddy schippt|2                                   |
    |Der 12jähirge Kriegsheld|2                                   |
    |Die Schwerter herhaus|2                                   |
    |Die Finsternis und ihr Eigentum|2                                   |


??? Example "Number of screening per city"
    === "SPARQL"
    ```sparql

    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX schema: <https://schema.org/>
    SELECT ?city ?country (COUNT(DISTINCT ?screening) AS ?n_screenings) WHERE {
    ?screening a schema:ScreeningEvent ;
        schema:location ?theater .

    ?theater a schema:MovieTheater ;
        schema:address ?theater_address .

    ?theater_address a schema:PostalAddress ;
        schema:addressLocality ?city ;
        schema:addressCountry ?country .
    } GROUP BY ?city ?country ORDER BY DESC(?n_screenings)

    ```

    Result:

    |city |country                             |n_screenings|
    |-----|------------------------------------|------------|
    |Örebro|SE                                  |19478       |
    |Moscow|SU                                  |13782       |
    |Rotterdam|NL                                  |13528       |
    |Amsterdam|NL                                  |13098       |
    |Gent |BE                                  |11061       |
    |Antwerpen|BE                                  |8971        |
    |Den Haag|NL                                  |8807        |
    |Leiden|NL                                  |5059        |
    |Arboga|SE                                  |4021        |
    |Groningen|NL                                  |3568        |
    |Kumla|SE                                  |3372        |
    |Utrecht|NL                                  |2934        |
    |Tilburg|NL                                  |1824        |
    |Hallsberg|SE                                  |1300        |
    |Hällefors|SE                                  |1091        |
    |Nijmegen|NL                                  |1028        |
    |Laxå |SE                                  |985         |
    |Eindhoven|NL                                  |969         |
    |Berchem|BE                                  |914         |
    |Haarlem|NL                                  |912         |
    |Deurne|BE                                  |818         |
    |Nora |SE                                  |798         |
    |Dordrecht|NL                                  |755         |
    |Alkmaar|NL                                  |725         |
    |Hoboken|BE                                  |529         |
    |Schiedam|NL                                  |521         |
    |Fjugesta|SE                                  |517         |
    |Zeist|NL                                  |510         |
    |'s Hertogenbosch|NL                                  |475         |
    |Tiel |NL                                  |465         |
    |Wilrijk|BE                                  |462         |
    |Metz |DE                                  |456         |
    |Frövi|SE                                  |437         |
    |Borgerhout|BE                                  |415         |
    |Askersund|SE                                  |406         |
    |Odensbacken|SE                                  |404         |
    |Fellingsbro|SE                                  |388         |
    |Gentbrugge|BE                                  |388         |
    |Apeldoorn|NL                                  |387         |
    |Merksem|BE                                  |362         |
    |Ledeberg|BE                                  |348         |
    |Vretstorp|SE                                  |329         |
    |Mortsel|BE                                  |324         |
    |Grythyttan|SE                                  |297         |
    |Melle|BE                                  |285         |
    |Sint-Amandsberg|BE                                  |264         |
    |Striberg|SE                                  |244         |
    |Östansjö|SE                                  |234         |
    |Hasselfors|SE                                  |230         |
    |Mullhyttan|SE                                  |214         |
    |Degerfors|SE                                  |180         |
    |Töreboda|SE                                  |177         |
    |Gyttorp|SE                                  |176         |
    |Eupen|BE                                  |167         |
    |Asker|SE                                  |163         |
    |Merelbeke|BE                                  |152         |
    |Sköllersta|SE                                  |147         |
    |Brevens Bruk|SE                                  |134         |
    |Hammar|SE                                  |122         |
    |Lanna|SE                                  |121         |
    |Finnerödja|SE                                  |117         |
    |Culemborg|NL                                  |115         |
    |Kilsmo|SE                                  |108         |
    |Sankt Vith|BE                                  |107         |
    |Vingåker|SE                                  |102         |
    |Lunger|SE                                  |101         |
    |Maastricht|NL                                  |100         |
    |Glanshammar|SE                                  |99          |
    |Åsbro|SE                                  |99          |
    |Mariakerke|BE                                  |94          |
    |Järnboås|SE                                  |94          |
    |Heerlen|NL                                  |92          |
    |Dalkarlsberg|SE                                  |87          |
    |Karlsborg|SE                                  |87          |
    |Karlskoga|SE                                  |86          |
    |Åmmeberg|SE                                  |85          |
    |Pershyttan|SE                                  |83          |
    |Aspa Bruk|SE                                  |75          |
    |Hova |SE                                  |73          |
    |Zwijndrecht|BE                                  |71          |
    |Schoten|BE                                  |67          |
    |Den Bosch|NL                                  |66          |
    |Lyrestad|SE                                  |61          |
    |Stora Mellösa|SE                                  |59          |
    |Evergem|BE                                  |58          |
    |Dyltabruk|SE                                  |57          |
    |Pålsboda|SE                                  |57          |
    |Rönneshytta|SE                                  |54          |
    |Hampetorp|SE                                  |53          |
    |Arnhem|NL                                  |51          |
    |Degerön|SE                                  |49          |
    |Delft|NL                                  |48          |
    |Zwijnaarde|BE                                  |46          |
    |Hengelo|NL                                  |44          |
    |Hoorn|NL                                  |39          |
    |Gavere|BE                                  |39          |
    |Kerkrade|NL                                  |33          |
    |Röfors|SE                                  |32          |
    |IJmuiden|NL                                  |29          |
    |Hilversum|NL                                  |28          |
    |Skyllberg|SE                                  |26          |
    |Kiel |BE                                  |26          |
    |Sittard|NL                                  |25          |
    |Mariedamm|SE                                  |25          |
    |Geleen|NL                                  |24          |
    |Zierikzee|NL                                  |23          |
    |NaN  |BE                                  |22          |
    |Malmedy|BE                                  |21          |
    |Wijnegem|BE                                  |15          |
    |Eke  |BE                                  |15          |
    |Echt (L)|NL                                  |14          |
    |Vlissingen|NL                                  |14          |
    |Simpelveld|NL                                  |12          |
    |Lovendegem|BE                                  |10          |
    |Breda|NL                                  |10          |
    |Hoensbroek|NL                                  |10          |
    |Brunssum|NL                                  |9           |
    |Beek (L)|NL                                  |8           |
    |Rijswijk (NH)|NL                                  |7           |
    |Valkenburg|NL                                  |7           |
    |Middelburg|NL                                  |6           |
    |Leeuwarden|NL                                  |5           |
    |Gemmenich |BE                                  |5           |
    |kumla|SE                                  |5           |
    |Bleyberg|BE                                  |4           |
    |Gulpen|NL                                  |4           |
    |Bergen op Zoom|NL                                  |4           |
    |Gemmenich|BE                                  |3           |
    |Enschede|NL                                  |3           |
    |Landgraaf (L)|NL                                  |2           |
    |Aubel|BE                                  |2           |
    |Kelmis|BE                                  |2           |
    |Ulflingen|BE                                  |2           |
    |Venlo|NL                                  |2           |
    |Bleyberg |BE                                  |2           |
    |Almelo|NL                                  |2           |
    |Brevensbruk|SE                                  |2           |
    |Recht|BE                                  |1           |
    |Verviers|BE                                  |1           |
    |Kampen|NL                                  |1           |
    |Helden-Panningen (L)|NL                                  |1           |


??? Example "Number of screenings per theater on a map"
    For some of the theaters, coordinate information was supplied. This means that we can plot these on the map. In here, we make use of the geo-plugin that is made available by [`https://yasgui.triply.cc/`](https://yasgui.triply.cc/). We therefore format our variables as `geo` (for the WKT geometry), `geoLabel` (for the popup with the number of screenings if you click the marker), and `geoTooltip` (for the name of the theater if you hover the marker). 

    === "SPARQL"
    ```sparql

    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX schema: <https://schema.org/>
    PREFIX geo: <http://www.opengis.net/ont/geosparql#>
    SELECT ?geoTooltip (COUNT(?screening) AS ?geoLabel) ?geo WHERE {
    ?screening a schema:ScreeningEvent ;
        schema:location ?theater .

    ?theater a schema:MovieTheater ;
        schema:name ?geoTooltip ;
        geo:hasGeometry/geo:asWKT ?geo .

    } GROUP BY ?theater ?geoTooltip ?geo

    ```
    
    Result:

    ![Example of Moscow theaters and the number of screenings](../data/example/map_example.png)

    

    The city and country names are included in this dataset as string (=text), conforming to the `schema.org` vocabulary's specification. In theory, we can model these as resources in the graph and connect them to a dataset that holds coordinates for all of these, by which we can also plot the cities on a map. 

??? Example "Number of films with/without external reference (IMDb and/or Wikidata)"
    === "SPARQL"
    ```sparql

    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX schema: <https://schema.org/>
    PREFIX owl: <http://www.w3.org/2002/07/owl#>
    SELECT (COUNT(?film) AS ?n_film) ?link_predicate WHERE {
    ?film a schema:Movie .

    # In our data, imdb references have been given with the `schema:sameAs` predicate.
    # References to Wikidata are modelled with the `owl:sameAs` prediate. 
    
    OPTIONAL { 
        ?film ?link_predicate ?external_resource . 
        
        FILTER(?link_predicate = schema:sameAs || ?link_predicate = owl:sameAs)
        FILTER(CONTAINS(STR(?external_resource), 'wikidata') || CONTAINS(STR(?external_resource), 'imdb'))
    } 

    } GROUP BY ?link_predicate

    ```

    Result:

    |n_film|link_predicate                      |
    |------|------------------------------------|
    |7044  |http://www.w3.org/2002/07/owl#sameAs|
    |23380 |https://schema.org/sameAs           |
    |1554  |                                    |

    In our data, imdb references have been given with the `schema:sameAs` predicate. References to Wikidata are modelled with the `owl:sameAs` prediate. 


### Cross dataset

??? Example "Number of screenings per film (total and per dataset)"
    === "SPARQL"
    ```sparql
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX schema: <https://schema.org/>
    #PREFIX geo: <http://www.opengis.net/ont/geosparql#>
    SELECT ?film (COUNT(DISTINCT ?screening) AS ?n_screenings)  WHERE {
    ?screening a schema:ScreeningEvent .
    ?screening schema:workPresented ?film .


    } GROUP BY ?film 
    ```

??? Example "Which films occur in more that one dataset?"
    === "SPARQL"
    ```sparql
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX schema: <https://schema.org/>
    SELECT (COUNT(DISTINCT ?dataset) AS ?n_datasets) ?imdb (GROUP_CONCAT(?film_name; separator="; ") AS ?names ) WHERE {
    GRAPH ?dataset {
        ?film a schema:Movie ;
        schema:name ?film_name ;
        schema:sameAs ?imdb .
    } 
    } GROUP BY ?imdb HAVING(COUNT(?dataset) > 1) ORDER BY DESC(?n_datasets)
    ```

    Result (top 20):

    |n_datasets|imdb                                |names                                                                                                                                               |
    |----------|------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------|
    |5         |https://www.imdb.com/title/tt0027977|Modern times; Modern Times; Modern times; Moderna tider; Modern times                                                                               |
    |5         |https://www.imdb.com/title/tt0026029|39 Steps, The; The 39 Steps; Het complot van 39 stappen; Komplot der 39 stappen; De 39 stegen; 39 Steps, The                                        |
    |5         |https://www.imdb.com/title/tt0027376|The Bohemian Girl; Bohemian girl, The; De zigeunerprinses; Zigenarflickan; Bohemian girl, The                                                       |
    |5         |https://www.imdb.com/title/tt0026778|A Night at the Opera; Night at the opera, A; Ben avond in de opera; Een avond in de opera; Galakväll på operan; Night at the opera, A               |
    |5         |https://www.imdb.com/title/tt0024601|Sons of the desert; Sons of the Desert; Kolder op zolder; Sons of the desert; Följ med oss till Honolulu; Sons of the desert                        |
    |5         |https://www.imdb.com/title/tt0026643|Lives of a Bengal Lancer, The; The Lives of a Bengal Lancer; De bengaalse lanciers; En bengalisk lansiär; Lives of a Bengal Lancer                  |
    |4         |https://www.imdb.com/title/tt0026138|Bride of Frankenstein, The; Bride of Frankenstein; Frankensteins brud; Bride of Frankenstein                                                        |
    |4         |https://www.imdb.com/title/tt0025907|Treasure Island; Treasure Island; Остров сокровищ; Treasure Island                                                                                  |
    |4         |https://www.imdb.com/title/tt0023969|Duck soup; Duck Soup; Fyra fula fiskar; Duck soup                                                                                                   |
    |4         |https://www.imdb.com/title/tt0026406|Ghost goes west, The; The Ghost Goes West; Spook te koop; Ghost goes west, The                                                                      |
    |4         |https://www.imdb.com/title/tt0015648|Bronenosets Potemkin; Potemkin; Броненосец Потемкин; Bronenosets Potyomkin                                                                          |
    |4         |https://www.imdb.com/title/tt0027125|Top hat; Top Hat; Top Hat; Top Hat                                                                                                                  |
    |4         |https://www.imdb.com/title/tt0027351|Bettelstudent, Der; Der Bettelstudent; Нищий студент; Bettelstudent, Der                                                                            |
    |4         |https://www.imdb.com/title/tt0026174|Captain Blood; Captain Blood; Kapten Blod; Captain Blood                                                                                            |
    |4         |https://www.imdb.com/title/tt0025603|Op hoop van zegen; Op hoop van zegen; Op hoop van zegen; Op hoop van zegen                                                                          |
    |4         |https://www.imdb.com/title/tt0024852|Babes in Toyland; Babes in Toyland; De wraak is zoet; Laurel en hardy als vagebonden; Laurel en hardy in wonderland; Wraak is zoet; Babes in Toyland|
    |4         |https://www.imdb.com/title/tt0029747|Way Out West; Een stad op stelten; Stad op stelten; Way out west; Vi reser västerut; Way out West                                                   |
    |4         |https://www.imdb.com/title/tt0026071|Anna Karenina; Anna Karenina; Anna Karenina; Anna Karenina                                                                                          |
    |4         |https://www.imdb.com/title/tt0024896|Bleeke Bet; Bleeke Bet; Bleke bet; Bleeke Bet                                                                                                       |
    |4         |https://www.imdb.com/title/tt0027194|Werewolf of London; Werewolf of London; Dr Yogami från London; Werewolf of London                                                                   |




??? Example "Which datasets have the most overlap?"
    === "SPARQL"
    ```sparql
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX schema: <https://schema.org/>
    SELECT ?dataset_combination (COUNT(*) AS ?count) WHERE {
    
    ?dataset_1 a schema:Dataset ;
        schema:name ?name_1 .
    ?dataset_2 a schema:Dataset ;
        schema:name ?name_2 .
    
    BIND(CONCAT(?name_1, ' + ', ?name_2) AS ?dataset_combination)
    
    GRAPH ?dataset_1 {
        ?film_1 a schema:Movie ;
        schema:sameAs ?imdb .
    } 
    
    GRAPH ?dataset_2 {
        ?film_2 a schema:Movie ;
        schema:sameAs ?imdb .
    } 
    
    FILTER(?dataset_1 != ?dataset_2)
    FILTER(STR(?dataset_1) > STR(?dataset_2))
    
    } GROUP BY ?dataset_combination ORDER BY DESC(?count)
    ```

    Results:

    |dataset_combination|count                               |
    |-------------------|------------------------------------|
    |Cinema Belgica + Screenings Amsterdam 1952, 1962 and 1972|1610                                |
    |Cinema Context + Cinema Belgica|1222                                |
    |Film Programming The Netherlands 1934-1936 + Cinema Context|1042                                |
    |Örebro films screened 1956-1958 and 1966-1968 + Cinema Belgica|847                                 |
    |Örebro films screened 1956-1958 and 1966-1968 + Screenings Amsterdam 1952, 1962 and 1972|590                                 |
    |Film Programming The Netherlands 1934-1936 + Cinema Belgica|431                                 |
    |Örebro films screened 1956-1958 and 1966-1968 + Cinema Context|85                                  |
    |Cinema Context + Screenings Amsterdam 1952, 1962 and 1972|75                                  |
    |Moscow film programming + Cinema Context|33                                  |
    |Moscow film programming + Cinema Belgica|30                                  |
    |Metz World War I + Cinema Context|27                                  |
    |Örebro films screened 1956-1958 and 1966-1968 + Film Programming The Netherlands 1934-1936|21                                  |
    |Film Programming The Netherlands 1934-1936 + Screenings Amsterdam 1952, 1962 and 1972|20                                  |
    |Film Programming The Netherlands 1934-1936 + Moscow film programming|11                                  |
    |Örebro films screened 1956-1958 and 1966-1968 + Moscow film programming|9                                   |
    |Moscow film programming + Screenings Amsterdam 1952, 1962 and 1972|6                                   |
    |Metz World War I + Cinema Belgica|2                                   |


### External data (Wikidata)

??? Example "Which Mexican/Russian/etc. films are in this data?"
    === "SPARQL"
    ```sparql
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX schema: <https://schema.org/>
    PREFIX wd: <http://www.wikidata.org/entity/>
    PREFIX wdt: <http://www.wikidata.org/prop/direct/>
    SELECT DISTINCT ?imdbURI (GROUP_CONCAT(?film_name; separator="; ") AS ?names ) WHERE {
    SERVICE <https://query.wikidata.org/sparql> {
        SELECT * WHERE {
        ?wdMovie wdt:P31 wd:Q11424 ;
            wdt:P495/wdt:P297 "MX" ;
            wdt:P345 ?imdbid . # imdb code 'tt0001234'
            BIND(URI(CONCAT("https://www.imdb.com/title/", ?imdbid)) AS ?imdbURI)  # join on this
        }
    }
    {
        SELECT * WHERE {
        ?film a schema:Movie ;
            schema:name ?film_name ;
            schema:sameAs ?imdbURI .  # join on this
        }
    
    }
    } GROUP BY ?imdbURI
    ```

    Results:

    |imdbURI|names                               |
    |-------|------------------------------------|
    |https://www.imdb.com/title/tt0044386|Bunuels the adventures of robinson crusoe; The adventures of robinson crusoe; Robinson Crusoe|
    |https://www.imdb.com/title/tt0254200|Cabaret Shangai                     |
    |https://www.imdb.com/title/tt0082257|Demonoid                            |
    |https://www.imdb.com/title/tt0041624|La malquerida                       |
    |https://www.imdb.com/title/tt0059719|Nazarin en simon de pilaarheilige   |
    |https://www.imdb.com/title/tt0054890|Nacht over parijs; The Four Horsemen of the Apocalypse|
    |https://www.imdb.com/title/tt0051504|La cucaracha                        |
    |https://www.imdb.com/title/tt0066049|A man called horse; A Man Called Horse|
    |https://www.imdb.com/title/tt0048045|De rebellenbruid; Uppror i Mexiko   |
    |https://www.imdb.com/title/tt0037981|La perla                            |
    |https://www.imdb.com/title/tt0053967|The young one                       |
    |https://www.imdb.com/title/tt0039793|Río Escondido                       |
    |https://www.imdb.com/title/tt0100432|Pueblo de madera                    |
    |https://www.imdb.com/title/tt0055408|Superman tegen de vampiervrouwen    |
    |https://www.imdb.com/title/tt0040755|Salon mexico; Salón México          |
    |https://www.imdb.com/title/tt0039742|Que Dios me perdone                 |
    |https://www.imdb.com/title/tt0036577|Las abandonadas                     |
    |https://www.imdb.com/title/tt0038473|La devoradora                       |
    |https://www.imdb.com/title/tt0138014|Pecadora                            |
    |https://www.imdb.com/title/tt0049521|Djungelmorden                       |
    |https://www.imdb.com/title/tt0038510|Enamorada; Enamorada; Enamorada     |
    |https://www.imdb.com/title/tt0052810|La fièvre monte à El Pao            |
    |https://www.imdb.com/title/tt0100746|La tarea                            |
    |https://www.imdb.com/title/tt0055601|Viridiana; Viridiana                |
    |https://www.imdb.com/title/tt0138090|Bekentenissen van een zondares; Sensualidad|
    |https://www.imdb.com/title/tt0082183|La chèvre                           |
    |https://www.imdb.com/title/tt0219298|El sexo fuerte                      |
    |https://www.imdb.com/title/tt0046233|Het strand                          |
    |https://www.imdb.com/title/tt0037054|Maria Candelaria                    |
    |https://www.imdb.com/title/tt0228811|San Francisco de Asís               |
    |https://www.imdb.com/title/tt0042804|Zij die vergeten worden; Zij, die vergeten worden; Los olvidados|
    |https://www.imdb.com/title/tt0101529|Cabeza de Vaca                      |
    |https://www.imdb.com/title/tt0029062|Jalisco nunca pierde                |
    |https://www.imdb.com/title/tt0247167|Amor en la sombra; Amor en la sombra; Amor en la sombra; Amor en la sombra; Amor en la sombra; Amor en la sombra; Amor en la sombra; Amor en la sombra|
    |https://www.imdb.com/title/tt0050712|Spelonk der verschrikking           |
    |https://www.imdb.com/title/tt0042971|María Magdalena, pecadora de Magdala|
    |https://www.imdb.com/title/tt0147166|Pervertida                          |
    |https://www.imdb.com/title/tt0051983|Nazarín                             |
    |https://www.imdb.com/title/tt0036722|Svarta piraten; El corsario negro   |
    |https://www.imdb.com/title/tt0051151|De vampier                          |
    |https://www.imdb.com/title/tt0218867|Camino de Sacramento                |
    |https://www.imdb.com/title/tt0047397|De hängdas revolution               |
    |https://www.imdb.com/title/tt0043115|Víctimas del pecado                 |


??? Example "Which actor is shown most often (in total and per dataset?)"
    === "SPARQL"
    ```sparql
    ```

??? Example "Which in Nazi-Germany (1933-1945) produced films are still screened after 1945. Where and when?"
    === "SPARQL"
    ```sparql
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX schema: <https://schema.org/>
    PREFIX wd: <http://www.wikidata.org/entity/>
    PREFIX wdt: <http://www.wikidata.org/prop/direct/>
    PREFIX xsd:  <http://www.w3.org/2001/XMLSchema#>
    SELECT DISTINCT ?imdbURI  (GROUP_CONCAT(DISTINCT ?film_name; separator="; ") AS ?names ) ?theater_name WHERE {
    SERVICE <https://query.wikidata.org/sparql> {
        SELECT * WHERE {
        ?wdMovie wdt:P31 wd:Q11424 ;
        wdt:P577 ?date ;  # Release date
        wdt:P495 wd:Q183 ;  # Germany
        wdt:P345 ?imdbid .
        
        FILTER (YEAR(?date) > 1938)
        FILTER (YEAR(?date) < 1946)
        
        BIND(URI(CONCAT("https://www.imdb.com/title/", ?imdbid)) AS ?imdbURI)  # join on this
        }
    }
    {
        SELECT * WHERE {
        ?screening a schema:ScreeningEvent ;
            schema:workPresented ?film ;
            schema:startDate ?startDate ;
            schema:location ?theater .
        
        ?theater a schema:MovieTheater ;
            schema:name ?theater_name .
        
        FILTER(?startDate > "1945-12-31"^^xsd:date)
        
        ?film a schema:Movie ;
            schema:name ?film_name ;
            schema:sameAs ?imdbURI .  # join on this
        }
      }
    } GROUP BY ?imdbURI ?theater_name
    ```

    Results (top 20):
 
    |imdbURI|names                               |theater_name          |
    |-------|------------------------------------|----------------------|
    |https://www.imdb.com/title/tt0037137|Замкнутый круг                      |Родина                |
    |https://www.imdb.com/title/tt0036249|Чудесный исцелитель                 |Ждановский ПКиО       |
    |https://www.imdb.com/title/tt0236249|Тибет                               |Динамо                |
    |https://www.imdb.com/title/tt0036519|Снежная фантазия                    |Форум                 |
    |https://www.imdb.com/title/tt0037137|Замкнутый круг                      |Смена                 |
    |https://www.imdb.com/title/tt0032702|Последний раунд                     |Шторм                 |
    |https://www.imdb.com/title/tt0031570|Восстание в пустыне                 |Метрополь             |
    |https://www.imdb.com/title/tt0032587|Дорога на эшафот                    |Луч                   |
    |https://www.imdb.com/title/tt0036519|Снежная фантазия                    |Москва                |
    |https://www.imdb.com/title/tt0033271|Возмездие                           |им. III Интернационала|
    |https://www.imdb.com/title/tt0032702|Последний раунд                     |Заря                  |
    |https://www.imdb.com/title/tt0210789|De kleine moek                      |De Liefde             |
    |https://www.imdb.com/title/tt0036249|Чудесный исцелитель                 |Шторм                 |
    |https://www.imdb.com/title/tt0032083|Знакомые мелодии                    |Мир                   |
    |https://www.imdb.com/title/tt0035248|Жизнь Рембрандта                    |Ударник               |
    |https://www.imdb.com/title/tt0036519|Снежная фантазия                    |Маяк                  |
    |https://www.imdb.com/title/tt0035540|Моцарт                              |Диск                  |
    |https://www.imdb.com/title/tt0135739|Воздушные акробаты                  |Победа                |
    |https://www.imdb.com/title/tt0032587|Дорога на эшафот                    |Таганский ПКиО        |
    |https://www.imdb.com/title/tt0032587|Дорога на эшафот                    |Ударник               |



??? Example "Get data on origin, cast, director and genre from Wikidata."
    === "SPARQL"
    ```sparql
    ```

??? Example "Get an image or a video of the described movies."
    === "SPARQL"
    ```sparql
    ```
