# Results

Each participant of the workshop had the possibility to send in their own data. The data of the ones that did have been converted to RDF and were used during the workshop. If permission was given, the original data (in csv) and the resulting RDF (in RDF/TriG) can be found below. 

## Datasets 

All datasets available during the workshop:

* Cinema Belgica ([metadata in json-ld](./data/datasets/metadata/cinemabelgica.jsonld))
* Cinema Context ([metadata in json-ld](./data/datasets/metadata/cinemacontext.jsonld))
* Film Club Events ([metadata in json-ld](./data/datasets/metadata/filmclub.jsonld))
* Film Programming The Netherlands 1934-1936 ([metadata in json-ld](./data/datasets/metadata/netherlands.jsonld))
* Historia de los públicos de cine en Buenos Aires ([metadata in json-ld](./data/datasets/metadata/buenosaires.jsonld))
* Metz World War I ([metadata in json-ld](./data/datasets/metadata/metzww1.jsonld))
* Moscow film programming ([metadata in json-ld](./data/datasets/metadata/moscow.jsonld))
* Örebro films screened 1956-1958 and 1966-1968 ([metadata in json-ld](./data/datasets/metadata/orebro.jsonld))
* Screenings Amsterdam 1952, 1962 and 1972 ([metadata in json-ld](./data/datasets/metadata/amsterdam.jsonld))

### Original spreadsheets (csv)

* Cinema Belgica ([csv](./data/datasets/original/workshop_lod_homer2022_cinemabelgica.csv))
* Cinema Context ([csv](./data/datasets/original/workshop_lod_homer2022_cinemacontext.csv))
* Film Programming The Netherlands 1934-1936 ([csv](./data/datasets/original/workshop_lod_homer2022_netherlands.csv))
* Metz World War I ([csv](./data/datasets/original/workshop_lod_homer2022_metzww1.csv))
* Moscow film programming ([csv](./data/datasets/original/workshop_lod_homer2022_moscow.csv))
* Örebro films screened 1956-1958 and 1966-1968 ([csv](./data/datasets/original/workshop_lod_homer2022_orebro.csv))
* Screenings Amsterdam 1952, 1962 and 1972 ([csv](./data/datasets/original/workshop_lod_homer2022_amsterdam.csv))

### RDF - Linked Open Data (RDF-TriG)

* Cinema Belgica ([RDF/TriG](./data/datasets/rdf/workshop_lod_homer2022_cinemabelgica.trig))
* Cinema Context ([RDF/TriG](./data/datasets/rdf/workshop_lod_homer2022_cinemacontext.trig))
* Film Programming The Netherlands 1934-1936 ([RDF/TriG](./data/datasets/rdf/workshop_lod_homer2022_netherlands.trig))
* Metz World War I ([RDF/TriG](./data/datasets/rdf/workshop_lod_homer2022_metzww1.trig))
* Moscow film programming ([RDF/TriG](./data/datasets/rdf/workshop_lod_homer2022_moscow.trig))
* Örebro films screened 1956-1958 and 1966-1968 ([RDF/TriG](./data/datasets/rdf/workshop_lod_homer2022_orebro.trig))
* Screenings Amsterdam 1952, 1962 and 1972 ([RDF/TriG](./data/datasets/rdf/workshop_lod_homer2022_amsterdam.trig))