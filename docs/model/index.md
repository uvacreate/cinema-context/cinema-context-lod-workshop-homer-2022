# Data Model

We use the [`schema.org`](http://schema.org/) vocabulary to describe our data in RDF. This vocabulary was developed to add structured data to websites so that they can be made more machine readable. Nowadays this vocabulary is increasingly being used to describe cultural heritage data as well. 

## Classes and relations

For the workshop we pick a limit number of classes and relations to describe our data so that we can compare the screenings of different films accross datasets. 

We will use the following classes and relations:

 **Classes** 

* `schema:ScreeningEvent`: a screening of a Movie
* `schema:Movie`: a movie
* `schema:MovieTheater`: a movie theater where the ScreeningEvent takes place

!!! info 
    The `schema:PostalAddress` and `geo:Geometry` (from the [GeoSPARQL ontology](http://www.opengis.net/ont/geosparql# )) classes are used to store grouped information (i.e. everything related to the address) in the graph, but are not given an explicit identifier. We model them as blank nodes.

**Relations**

* `schema:workPresented`: points to the movie that is presented in the ScreeningEvent
* `schema:location`: points to the MovieTheater where the ScreeningEvent takes place
* `schema:sameAs`: points to the (external) IMDb page of the Movie (e.g. [https://www.imdb.com/title/tt0014646](https://www.imdb.com/title/tt0014646)
* `owl:sameAs`: points to exactly the same (external) Movie described in WikiData (e.g. [http://www.wikidata.org/entity/Q380803](http://www.wikidata.org/entity/Q380803)

## Schematic overview

```mermaid
graph TD

  classDef Literal fill:transparent,stroke-width:0px;

  schema:ScreeningEvent("schema:ScreeningEvent") -- schema:location ---> schema:MovieTheater
  
  schema:ScreeningEvent -- schema:workPresented ---> schema:Movie

  schema:MovieTheater("schema:MovieTheater") -- schema:name ---> theaterName("#quot;Name of the movie theater#quot;"):::Literal
  schema:MovieTheater -- schema:address ----> schema:PostalAddress("schema:PostalAddress")

  schema:MovieTheater -- geo:hasGeometry --> geo:Geometry("geo:Geometry")

  schema:ScreeningEvent -- schema:startDate ---> date("#quot;YYYY-mm-dd#quot;"):::Literal
  
  schema:Movie("schema:Movie") -- schema:name ---> movieName("#quot;Name of the movie#quot;"):::Literal
  schema:Movie -- owl:sameAs ---> Wikidata("Wikidata URI")
  schema:Movie -- schema:sameAs ---> IMDb("IMDb URL")

```

## Example in RDF (turtle)

For those already familiar with rdf: below is an example of a single screening event modelled in RDF. 

=== "Turtle (.ttl)"
```turtle

PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX geo: <http://www.opengis.net/ont/geosparql#>
PREFIX schema: <http://schema.org/>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

<http://www.cinemacontext.nl/id/V078360#1> a schema:ScreeningEvent ;
  schema:location <http://www.cinemacontext.nl/id/B000071> ;
  schema:startDate "1911-02-04"^^xsd:date ;
  schema:workPresented <http://www.cinemacontext.nl/id/F031013> .

<http://www.cinemacontext.nl/id/B000071> a schema:MovieTheater ;
  schema:name "Americain Bioscoop" ;
  schema:address [ a schema:PostalAddress ;
                  schema:addressCountry "NL" ;
                  schema:addressLocality "Amsterdam" ;
                  schema:streetAddress "Daniël Stalpertstraat 67" ] ;
  geo:hasGeometry [ a geo:Geometry ;
                    geo:asWKT "POINT(4.8903107 52.3563093)"^^geo:wktLiteral ] .

<http://www.cinemacontext.nl/id/F031013> a schema:Movie ;
  schema:name "De Sheriff"@nl ;
  schema:sameAs <https://www.imdb.com/title/tt0354912> .


```
